# Plugin templates for Windows Server Advanced Power Management
[Windows Server Advanced Power Management (WSAPM)](https://codeberg.org/DecaTec/Wsapm) is an application for advanced power management on Windows (home) servers.

This repository contains templates for WSAPM plugins:
- **WsapmPluginTemplate:** Template for a simple plugin (without settings and UI)
- **WsapmPluginAdvancedTemplate:** Template for an advanced plugin( with settings and UI) - using WPF
- **WsapmPluginAdvancedTemplateWinForms:** Template for an advanced plugin( with settings and UI) - using WinForms

For more information, please see the [Git repository of Windows Server Advanced Power Management](https://codeberg.org/DecaTec/Wsapm)
